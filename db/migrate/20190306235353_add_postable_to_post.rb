class AddPostableToPost < ActiveRecord::Migration[5.2]
  change_table :posts do |t|
    t.references :postable, polymorphic: true
  end
end
