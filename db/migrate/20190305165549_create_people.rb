class CreatePeople < ActiveRecord::Migration[5.2]
  def change
    create_table :people do |t|
      t.string :name
      t.string :last_name
      t.date :birthday
      t.string :genre
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
