class CreateSubscriptions < ActiveRecord::Migration[5.2]
  def change
    create_table :subscriptions do |t|
      t.integer :user_id
      t.integer :user_subs_id
      t.date :subs_date

      t.timestamps
    end

    add_index :subscriptions, :user_id
    add_index :subscriptions, :user_subs_id
    add_index :subscriptions, [:user_id, :user_subs_id], unique: true
  end
end
