class AddPostObjectIdToComments < ActiveRecord::Migration[5.2]
  def change
    add_column :comments, :post_object_id, :string
  end
end
