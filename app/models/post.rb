class Post < ApplicationRecord
    include Mongoid::Document
    include Mongoid::Timestamps::Created
    include Mongoid::Timestamps::Updated
    include Mongoid::Attributes::Dynamic
    # rolify
    # validates :title, :content, :presence => true
    # validates :title, :content, :tags, :presence => true

    #embeds_many :comments
    # has_many :comments

    #has_and_belongs_to_many :categories

    #belongs_to :postable, polymorphic: true

    #accepts_nested_attributes_for :postable

    def user
        User.find(self.postable_id)
    end

    def company
        Company.find(self.postable_id)
    end

    def categories
        Category.where(id: self.category_ids)
    end

    def comments
        Comment.where(post_object_id: self._id.to_s)
    end

end
