class Comment < ApplicationRecord
  mount_uploader :image, ImageUploader
  #belongs_to :post
  #belongs_to :user

  def user
    User.find(self.user_id)
  end

  def post
    Post.find(self.post_object_id)
  end

end
