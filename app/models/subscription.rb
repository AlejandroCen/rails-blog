class Subscription < ApplicationRecord
  belongs_to :user
  belongs_to :subscriber, :foreign_key => "user_subs_id", :class_name => "User"
end
