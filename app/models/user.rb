class User < ApplicationRecord
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_one :person
  has_many :subscriptions
  has_many :users, through: :subscriptions
  has_many :companies
  has_many :posts, :as => :postable

  accepts_nested_attributes_for :person
end
