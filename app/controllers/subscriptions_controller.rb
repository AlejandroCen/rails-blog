class SubscriptionsController < ApplicationController
  before_action :authenticate_user!

  def index
    @suscribers = current_user.users

    @subscriptions = Subscription.all
  end

  def create
    @subscription = Subscription.new(subscription_params)
    @subscription.user_subs_id = current_user.id
    @subscription.subs_date = Date.today
    respond_to do |format|
      if @subscription.save
        format.html { redirect_to posts_path, notice: 'Subscription was successfully created.' }
      end
    end
  end

  private
  def subscription_params
    params.require(:subscription).permit(:user_id)
  end
end
