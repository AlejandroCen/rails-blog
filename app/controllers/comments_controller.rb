class CommentsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def create
    # Obtener el id del post y crear el comentario
    @comment = Comment.new(comment_params)
    @comment.user_id = current_user.id

    respond_to do |format|
      if @comment.save
        json = { :user => @comment.user.person, :comment => @comment }.to_json
        format.json { render json: json }
      end
    end
  end

  private
    def comment_params
      params.require(:comment).permit(:content, :post_object_id, :image)
    end

end
