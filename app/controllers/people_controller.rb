class PeopleController < ApplicationController
  before_action :authenticate_user!

  def index
    @profile = current_user
  end

  def update
    respond_to do |format|
      if current_user.person.update(person_params)
        format.html { redirect_to people_path, notice: 'Avatar was successfully updated.' }
      end
    end
  end

  private
    def person_params
      params.require(:person).permit(:avatar)
    end

end
