// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

document.addEventListener('DOMContentLoaded', function(event) {
    $('#createComment').hide();
    $('#companiesSelect').hide();

    $('#btnComment').click(function() {
        let isVisible = $('#createComment').is(':visible');
        if (isVisible) $('#createComment').hide();
        else {
            $('#btnComment').hide();
            $('#createComment').show();
        }
    });

    $('#companyRadio').click(function() {
        if($(this).is(':checked')) $('#companiesSelect').show();
    });

    $('#userRadio').click(function() {
        if($(this).is(':checked')) $('#companiesSelect').hide();
    });

    $('#new_comment').submit(function(event) {
        event.preventDefault();
        const formData = new FormData();
        const post_id = $("#comment_post_id").val();
        const content = $("#comment_content").val();
        const image = $('#comment_image');

        formData.append('comment[image]', image[0].files[0]);
        formData.append('comment[post_object_id]', post_id);
        formData.append('comment[content]', content);

        $.ajax({
            type: "POST",
            url: "/comments",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                // alert(JSON.stringify(data));
                $("#comments").prepend(
                    '<div class="card">' +
                        '<div style="max-width: 80px;">' +
                            '<img class="card-img-top" src="' + data.comment.image.url  + '" alt="Card image cap">' +
                        '</div>' +
                        '<div class="card-header">' +
                            data.user.name + ' ' + data.user.last_name +
                        '</div>' +
                        '<div class="card-body">' +
                            data.comment.content +
                            '<div class="badge badge-secondary" style="float: right;">' +
                                data.comment.created_at +
                            '</div>' +
                        '</div>' +
                    '</div>'
                );
            },
            error: function(res){
                alert(JSON.stringify(res));
            }
        })
    });
});