// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
document.addEventListener('DOMContentLoaded', function(event) {
    $('#new_subscription').submit(function() {
        const user_id = $("#subscription_user_id").val();
        $.ajax({
            type: "POST",
            url: "/subscriptions",
            data: { user_id: user_id },
            success: function(res) {
                alert('Subscrito');
            },
            error: function(res) {
                alert('Ya subscrito');
            }
        })
    });
});