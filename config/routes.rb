Rails.application.routes.draw do
  resources :companies
  devise_for :users, :controllers => {:registrations => "users/registrations"}
  resources :posts
  resource :comments
  resources :categories
  resources :subscriptions
  resources :people, :path => 'profile'
  root :to => "posts#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
